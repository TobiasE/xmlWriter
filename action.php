<?php  
    
// Die verschiedenen Arrays
// Try-Catch Block
$datumVon = array($_POST['fdatumVon_0'], $_POST['fdatumVon_1'], $_POST['fdatumVon_2'], $_POST['fdatumVon_3'], $_POST['fdatumVon_4'], $_POST['fdatumVon_5']);
$datumBis = array($_POST['fdatumBis_0'], $_POST['fdatumBis_1'], $_POST['fdatumBis_2'], $_POST['fdatumBis_3'], $_POST['fdatumBis_4'], $_POST['fdatumBis_5']);
$title = array($_POST['ftitle_0'], $_POST['ftitle_1'], $_POST['ftitle_2'], $_POST['ftitle_3'], $_POST['ftitle_4'], $_POST['ftitle_5']);
$subline = array($_POST['fsubline_0'], $_POST['fsubline_1'], $_POST['fsubline_2'], $_POST['fsubline_3'], $_POST['fsubline_4'], $_POST['fsubline_5']);
$ortOben = array($_POST['fortOben_0'], $_POST['fortOben_1'], $_POST['fortOben_2'], $_POST['fortOben_3'], $_POST['fortOben_4'], $_POST['fortOben_5']);
$ortUnten = array($_POST['fortUnten_0'], $_POST['fortUnten_1'], $_POST['fortUnten_2'], $_POST['fortUnten_3'], $_POST['fortUnten_4'], $_POST['fortUnten_5']);

//Läd das Master XML --> Hier das korrekte laden 
$xml=simplexml_load_file("de_vorlage_final.xml") or die("Error: Cannot create object");

// Reset den XML-Durchlauf
$innereSchleife = 0;
$aeussereSchleife = 0;

// Befüllen des XML
for ($innereSchleife = 0; $innereSchleife <= 5; $innereSchleife++){
    $xml->texte[0]->feld[$aeussereSchleife]['text'] = $datumVon[$innereSchleife];
    $aeussereSchleife++;
    $xml->texte[0]->feld[$aeussereSchleife]['text'] = $datumBis[$innereSchleife];
    $aeussereSchleife++;
    $xml->texte[0]->feld[$aeussereSchleife]['text'] = $title[$innereSchleife];
    $aeussereSchleife++;
    $xml->texte[0]->feld[$aeussereSchleife]['text'] = $subline[$innereSchleife];
    $aeussereSchleife++;
    $xml->texte[0]->feld[$aeussereSchleife]['text'] = $ortOben[$innereSchleife];
    $aeussereSchleife++;
    $xml->texte[0]->feld[$aeussereSchleife]['text'] = $ortUnten[$innereSchleife];
    $aeussereSchleife++;
}

// Datei Überschreiben
$dom = new DOMDocument('1.0');
$dom->preserveWhiteSpace = false;
$dom->formatOutput = true;
$dom->loadXML($xml->asXML());

// Richtige Datei schreiben
$pickValue = ($_POST['flughafenPick']);
$pickShortn = "";
if($pickValue == 'Düsseldorf'){
    $pickShortn = "dus_";
} elseif($pickValue == 'München'){
    $pickShortn = "muen_";
} elseif($pickValue == 'Nürnberg'){
    $pickShortn = "nuern_";
} elseif($pickValue == 'Berlin'){
    $pickShortn = "ber_";
} elseif($pickValue == 'Hannover'){
    $pickShortn = "hann_";
} elseif($pickValue == 'Hamburg'){
    $pickShortn = "ham_";
} else {
    echo "Fehler im Dropdown!";
}
// Speichern
$dom->save($pickShortn . 'content.xml');

//AUSGABE
echo ("Daten erfolgreich uebertragen!");
    
?>

<!-- RELATIVER LINK!!! -->
<a href="index_dropdown.html">Go back to the Main Page</a>.
    